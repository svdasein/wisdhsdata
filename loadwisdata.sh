#!/bin/bash
#wget -O wiscorona_county.csv "https://opendata.arcgis.com/datasets/5374188992374b318d3e2305216ee413_12.csv"
wget -O wiscorona_county.csv "https://opendata.arcgis.com/api/v3/datasets/6496bdf64f7a44bba593862d04d01b2b_12/downloads/data?format=csv&spatialRefId=3857"
iconv -c -f utf-8 -t ascii  wiscorona_county.csv > wisdata.csv
./loadwisdata.rb wisdata.csv 
cp wisdata.csv backup/wisdata-dhs-`date +%u`.csv
influx -database=wisdata -format=json -pretty=true -execute='SELECT * FROM /.*/' > backup/wisdata-backup-`date +%u`.json
influx -database=wisdata -format=csv -pretty=true -execute='SELECT * FROM /.*/' > backup/wisdata-backup-`date +%u`.csv
cd backup
git add -A
git commit -am "automatic commit of backupdir contents"
git push
