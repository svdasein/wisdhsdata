#!/usr/bin/env ruby
require 'set'
require 'csv'
require 'influxdb'
require 'redis'
require 'pry'
require 'ruby-progressbar'

useless = ['OBJECTID','GEO','GEOID','Shape__Area','Shape__Length','RptDt'].to_set

wisdata = CSV.read(ARGV[0],headers: true)
influx = InfluxDB::Client.new('wisdatav2')
redis = Redis.new

puts "Newest data in this dataset: #{wisdata.collect { |each| DateTime.parse(each['Date']).to_time.localtime }.max}"
earliestTime = DateTime.parse(influx.query('select last(DTH_CUM_CP) from /.*/')[0]['values'][0]['time']).to_time.localtime
newdata = wisdata.select { |each| DateTime.parse(each['Date']).to_time.localtime > earliestTime }
puts "Loading data after #{Time.at(earliestTime)} = #{newdata.size} records in dataset"
#newdata = wisdata

progress = ProgressBar.create(:title => "Loading record ",:length=>80,:format => '%t %c/%C (%p%%) |%B|%e', :starting_at => 0, :total => newdata.size)

newdata.each {|rec|
  progress.increment

  data = rec.to_h.delete_if {|k,v|v.nil? || v == "-999" || useless.include?(k)}
  county = data['GEOName']
  data.delete('GEOName')
  timestamp = DateTime.parse(rec['Date']).to_time.to_i
  data.delete('DATE')
  data.delete('Date')
  data.delete('RptDt')
  data.delete('OBJECTID')
  data.transform_values!(&:to_f)

  popEst = redis.hget('Wisconsin-population',county).to_f
  area = redis.hget('Wisconsin-area',county).to_f
  density = redis.hget('Wisconsin-density',county).to_f
  if popEst > 0 && data['POS_CP'] && data['NEG']
    totTested = data['POS_CP'] + data['NEG']
    data['TotalTests'] = totTested
    data['PopEst'] = popEst
    data['PctPopTested'] = totTested / popEst if popEst > 0
    data['PctPopPositive'] = data['POS_CP'] / popEst if popEst > 0
    data['areaSqMi'] = area
    data['densitySqMi'] = density
    data['PosNegRatio'] = data['POS_CP'] / totTested if totTested > 0
  end

  influx.write_point('counties',{
        timestamp: timestamp,
        tags: {county: county},
        values: data
  })
}

puts
