#!/usr/bin/env ruby
require 'set'
require 'csv'
require 'influxdb'
require 'redis'
require 'pry'
require 'awesome_print'
require 'ruby-progressbar'

@useless = ['OBJECTID','GEO','GEOID','Shape__Area','Shape__Length'].to_set

wisarchive = CSV.read(ARGV[0],headers: true)
recsInArchive = 8496
wisdata = CSV.read(ARGV[1],headers: true)
@influx = InfluxDB::Client.new('wisdata')
@influx.delete_database('wisdata')
@influx.create_database('wisdata')
@redis = Redis.new



def processRecord(rec)
  data = rec.to_h.delete_if {|k,v|v.nil? || @useless.include?(k)}
  ['POSITIVE','NEGATIVE','DEATHS'].each { |field|
      data[field] = data[field].to_f if data[field]
      data[field] = 0.0 if (not data[field].nil?) and (data[field] < 0)
  }
  county = data['NAME']
  data.delete('NAME')
  #timestamp = Time.at(rec['LoadDttm'].to_i/1000).to_i
  #timestamp = DateTime.parse(rec['LoadDttm']).to_time.to_i
  #data.delete('LoadDttm')
  timestamp = DateTime.parse(rec['DATE']).to_time.to_i
  data.delete('DATE')
  #binding.pry
  data.delete('OBJECTID')

  popEst = @redis.hget('Wisconsin-population',county).to_f
  area = @redis.hget('Wisconsin-area',county).to_f
  density = @redis.hget('Wisconsin-density',county).to_f
  if popEst > 0 && data['POSITIVE'] && data['NEGATIVE']
    totTested = data['POSITIVE'] + data['NEGATIVE']
    data['TotalTests'] = totTested
    data['PopEst'] = popEst
    data['PctPopTested'] = totTested / popEst
    data['PctPopPositive'] = data['POSITIVE'] / popEst
    data['areaSqMi'] = area
    data['densitySqMi'] = density
    data['PosNegRatio'] = data['POSITIVE'] / totTested
  end



  @influx.write_point('counties',{
        timestamp: timestamp,
        tags: {county: county},
        values: data
  })
  return timestamp
end

earliestTime = Time.now.to_i
progress = ProgressBar.create(:title => "Loading record ",:length=>80,:format => '%t %c/%C (%p%%) |%B|%e', :starting_at => 0, :total => wisdata.size)
wisdata.each {|rec|
  progress.increment
  ts = processRecord(rec)
  earliestTime = ts if ts < earliestTime
}

archiveEndDate = Time.at(earliestTime).to_date.to_time
#=begin
wisarchive.each {|rec|
  timestamp = DateTime.parse(rec['DATE']).to_time
  if timestamp < archiveEndDate
    print '.'
    processRecord(rec)
  end
}
#=end
puts
